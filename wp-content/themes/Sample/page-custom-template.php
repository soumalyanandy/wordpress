<?php
/*
Template Name: My Custom Template
*/
 get_header(); ?>
<!-- <h1>This is my index.</h1> -->
<?php 
	if(have_posts()): 
		while(have_posts()): the_post();
?>
			<h1>This is my static title.</h1>
			<h3><?php the_title();?></h3>
			-----------------------------------------------
			<p><?php the_content();?></p>
			<hr/>
<?php 
		endwhile;
	endif;
?>
<?php get_footer(); ?>