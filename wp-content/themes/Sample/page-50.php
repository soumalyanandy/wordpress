<?php get_header(); ?>
<!-- <h1>This is my index.</h1> -->
<?php 
	if(have_posts()): 
		while(have_posts()): the_post();
?>
			<p><?php the_content();?></p>
			<h3><?php the_title();?></h3>
			<hr/>
<?php 
		endwhile;
	endif;
?>
<?php get_footer(); ?>