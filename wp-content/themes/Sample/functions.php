<?php

function sample_script_enqueue(){
	wp_enqueue_style('customstyle', get_template_directory_uri()."/css/sample.css", array(), '1.0.0', 'all');
	// load js in footer
	wp_enqueue_script('customjs', get_template_directory_uri()."/js/sample.js", array(), '1.0.0', true);
}

add_action('wp_enqueue_scripts', 'sample_script_enqueue');

function sample_theme_setup(){
	add_theme_support("menus");

	register_nav_menu('primary', 'Primary Header Navigation');
	register_nav_menu('secondary', 'Footer Navigation');
}

//after_setup_theme
add_action('init', 'sample_theme_setup');

// There are 10 theme support options .
add_theme_support('custom-background'); // for custom background
add_theme_support('custom-header'); //for custom header
add_theme_support('post-thumbnails'); // for post/page feature image.