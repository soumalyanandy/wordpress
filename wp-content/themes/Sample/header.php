<!doctype html>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Awesome theme</title>
	<?php wp_head(); ?>
</head>
<?php 
	if( is_front_page() ): 	//is_home() -> check for blog page which is default home page.
							//is_front_page() -> check for custom home page.
		$awesome_class = array( 'awesome_class', 'my_class' );
	else:
		$awesome_class = array( 'not_awesome_class' );
	endif;
?>
<body <?php body_class($awesome_class); ?>>

	<?php wp_nav_menu(array(
			'theme_location' => 'primary'
		)); 
		//var_dump(get_custom_header());
	?>
<img src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" />

